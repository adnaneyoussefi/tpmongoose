var express = require('express');
var router = express.Router();
const Livre = require('../models/Livre');

/* GET home page. */
router.get('/', async function(req, res, next) {
  try {
    var livres = await Livre.find();
  }
  catch(e) {
    console.log(e)
  }
  res.render('index', { livres });
});

router.get('/show', function(req, res, next) {
  res.render('show', { title: 'Create' });
});

router.post('/add', async function(req, res, next) {
  let { titre, content, author } = req.body;
  const livre = new Livre({
    titre, content, author
});
  try {
    await livre.save().then(() => console.log('Livre added'));
  } catch(e) {
    console.log(e)
  }
  res.render('index', { title: 'Create' });
});

router.get('/update', function(req, res, next) {
  res.render('update', { title: 'Create' });
});

router.get('/delete', function(req, res, next) {
  res.render('delete', { title: 'Create' });
});

router.get('/insert', function(req, res, next) {
  res.render('insert', { title: 'Create' });
});

module.exports = router;
