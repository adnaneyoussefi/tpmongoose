const mongoose = require('mongoose')

const livreSchema = new mongoose.Schema({
    titre: {
        type: String
    },
    content: {
        type: String
    },
    author: {
        type: String
    }
})

module.exports = mongoose.model('Livre', livreSchema);